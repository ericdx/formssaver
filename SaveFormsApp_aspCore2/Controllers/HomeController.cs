﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ClassLibrary;
using ClassLibrary.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SaveFormsApp_aspCore2.Models;

namespace SaveFormsApp_aspCore2.Controllers
{
    public class HomeController : Controller
    {
        public FormsContext _formsContext { get; private set; }

        public HomeController(FormsContext formsContext)
        {
            _formsContext = formsContext;
        }
        public IActionResult Index()
        {
            return View("Form");
        }

        [HttpPost]
        public IActionResult SaveForm(FormModel form)
        {
            string savingResult = null;

            try
            {
                _formsContext.Forms.Add(form);
                _formsContext.SaveChanges();
            }
            catch (Exception e)
            {
                savingResult = e.InnerException?.Message;
            }
            return PartialView("_FormSavingResult", savingResult);
        }

        [Authorize]
        public IActionResult Admin()
        {
            var model = new FormsTableModel(_formsContext.Forms.ToList());
            return View(model);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
