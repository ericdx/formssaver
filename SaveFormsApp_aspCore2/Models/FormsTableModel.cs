﻿using ClassLibrary.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaveFormsApp_aspCore2.Models
{
    public class FormsTableModel
    {
        public FormsTableModel(List<FormModel> tableRows)
        {
            TableRows = tableRows;
        }

        public List<FormModel> TableRows { get; private set; }
        public SelectList FirstNames => new SelectList(TableRows.Select(tr => tr.FirstName).Distinct());
        public SelectList LastNames => new SelectList(TableRows.Select(tr => tr.LastName).Distinct());
        public SelectList BirthDays => new SelectList(TableRows.Select(tr => tr.BirthDay).Distinct());
        public SelectList Phones => new SelectList(TableRows.Select(tr => tr.Phone).Distinct());
        public SelectList Colors => new SelectList(TableRows.Select(tr => tr.Color).Distinct());
        public SelectList Drinks => new SelectList(TableRows.Select(tr => tr.Drink).Distinct());


    }
}
