﻿using ClassLibrary.Data;
using Microsoft.EntityFrameworkCore;
using System;

namespace ClassLibrary
{
    public class FormsContext : DbContext
    {
        public DbSet<FormModel> Forms { get; set; }

        public FormsContext(DbContextOptions<FormsContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
